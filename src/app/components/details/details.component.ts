import { Component, OnInit } from '@angular/core';
import {DashboardService} from '../../services/dashboard.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {

  quote: any;

  constructor(private dashboardSvc: DashboardService, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.dashboardSvc.getQuoteById(this.activatedRoute.snapshot.params.id).subscribe(resp => {
      this.quote = resp;
    });
  }

}
