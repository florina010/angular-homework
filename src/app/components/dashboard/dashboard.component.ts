import {Component, OnInit} from '@angular/core';
import {DashboardService} from '../../services/dashboard.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  pageIndex = 1;
  totalRecords = 50;
  size = 10;
  from = 0;
  to = 0;
  quotes = [];
  loading = false;
  tags = [];
  selectedTag = '';

  constructor(private dashboardSvc: DashboardService, private route: ActivatedRoute, private router: Router) {
  }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      const {page, ...filters} = params;
      if (page > 0) {
        this.dashboardSvc.setPage(page);
        this.pageIndex = page;
      } else {
        this.dashboardSvc.setPage(1);
      }
      if (filters) {
        this.dashboardSvc.setFilters(filters);
      }
      this.selectedTag = this.dashboardSvc.filters.tags || 'All';
      this.getTags();
      this.getQuotes(page);
    });
  }

  getQuotes(page: number): void {
    this.quotes.length = 0;
    this.loading = true;
    this.dashboardSvc.getQuotes(page).subscribe(resp => {
      this.quotes = resp.results;
      this.totalRecords = resp.totalCount;
      this.from = this.pageIndex === 1 ? this.pageIndex : (this.pageIndex - 1) * this.size + 1;
      this.to =
        this.size * this.pageIndex > this.totalRecords ? this.totalRecords : this.size * this.pageIndex;
      this.loading = false;
    });
  }

  getTags(): void {
    this.dashboardSvc.getTags().subscribe(resp => {
      this.tags = resp;
    });
  }

  setTag(event: any): void {
    this.pageIndex = 1;
    this.dashboardSvc.setPage(this.pageIndex);
    this.dashboardSvc.setFilters({tags: event.target?.value});
  }

  goToQuote(quote: any): void {
    this.router.navigate([`/details/${quote._id}`]);
  }

  goToNextPage(): void {
    if (this.pageIndex <= this.totalRecords / this.size) {
      this.pageIndex = Number(this.pageIndex) + 1;
      this.dashboardSvc.setPage(this.pageIndex);
    }
  }

  goToPreviousPage(): void {
    if (this.pageIndex > 1) {
      this.pageIndex = Number(this.pageIndex) - 1;
      this.dashboardSvc.setPage(this.pageIndex);
    }
  }

  updateManualPage(event: any): void {
    const index = event.target.value;
    if (index > 0 && index <= this.totalRecords / this.size) {
      this.pageIndex = index;
    } else {
      this.pageIndex = index < 0 ? 1 : Math.ceil(this.totalRecords / this.size);
    }
    this.dashboardSvc.setPage(this.pageIndex);
  }

}
