import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: object;

  constructor(public router: Router) {
    this.loginForm = {email: true, password: true, confirmPassword: false};

  }

  ngOnInit(): void { }

}
