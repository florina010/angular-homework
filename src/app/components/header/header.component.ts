import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from '../../services/authentication.service';
import {Router, RoutesRecognized} from '@angular/router';
import {DashboardService} from '../../services/dashboard.service';
import {filter, pairwise} from 'rxjs/operators';
import {Location} from '@angular/common';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  isLoggedIn = false;
  detailsPage = false;

  constructor(private authSvc: AuthenticationService,
              private router: Router,
              private dashboardSvc: DashboardService,
              private location: Location) {
    router.events.pipe(filter(event => event instanceof RoutesRecognized), pairwise())
      .subscribe((routes) => {
        this.isLoggedIn = this.authSvc.isLoggedIn();
        this.detailsPage = Object.values(routes[1]).some(value => typeof value === 'string' && value.indexOf('details') !== -1);
      });
  }

  ngOnInit(): void {
    this.isLoggedIn = this.authSvc.isLoggedIn();
    this.detailsPage = this.location.path().includes('details');
  }

  goToDashboard(): void {
    const queryParams = {page: this.dashboardSvc.page, ...this.dashboardSvc.filters};
    this.router.navigate(['./dashboard'], {queryParams});
  }

  logout(): void {
    this.authSvc.logout();
  }

}
