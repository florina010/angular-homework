import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  registerForm: object;

  constructor(public router: Router) {
    this.registerForm = {email: true, password: true, confirmPassword: true};
  }

  ngOnInit(): void {
  }

}
