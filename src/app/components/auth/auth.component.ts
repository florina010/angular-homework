import {Component, Input, OnInit} from '@angular/core';
import {AuthenticationService} from '../../services/authentication.service';
import {Router} from '@angular/router';
import {MustMatch} from 'src/app/utils/validators/must-match.validator';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {EMAIL_PATTERN, PASSWORD_PATTERN} from '../../utils/validators/validators.patterns';


@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {

  @Input() formData: any;
  showPassword = false;
  showConfirmPassword = false;
  loading = false;
  form: FormGroup | undefined;

  constructor(private authSvc: AuthenticationService, public router: Router, private formBuilder: FormBuilder) {
  }

  ngOnInit(): void {
    this.formData = JSON.parse(this.formData);
    if (!this.formData.confirmPassword) {
      this.form = this.formBuilder.group({
        email: ['', [Validators.required, Validators.pattern(EMAIL_PATTERN)]],
        password: ['', [Validators.required, Validators.pattern(PASSWORD_PATTERN)]]
      });
    } else {
      this.form = this.formBuilder.group({
        email: ['', [Validators.required, Validators.pattern(EMAIL_PATTERN)]],
        password: ['', [Validators.required, Validators.pattern(PASSWORD_PATTERN)]],
        confirmPassword: ['', [Validators.required]]
      }, {validators: MustMatch('password', 'confirmPassword')});
    }
  }


  onSubmit(): void {
    if (this.form?.invalid) {
      return;
    }

    this.loading = true;
    const {email, password} = this.form?.value;
    const fn = this.formData.confirmPassword ? this.authSvc.register : this.authSvc.login;
    setTimeout(() => {
      fn(email, password).subscribe((resp) => {
        this.loading = false;
        if (resp.error) {
          alert(resp.error);
        }
        this.router.navigate(['/dashboard'], {queryParams: {page: 1}});
      }, () => {
        this.loading = false;
      });
    }, 1000);
  }
}
