import { Component } from '@angular/core';
import {Title} from '@angular/platform-browser';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(private titleSvc: Title,  private activatedRoute: ActivatedRoute) {
    console.log(this.activatedRoute.snapshot.data)    // const pageTitle = {...activatedRoute, title: pageTitle} || 'HdPay Admin';
    // this.titleSvc.setTitle(pageTitle);
  }
}
