import { Injectable } from '@angular/core';
import {Observable, of} from 'rxjs';
import {Router} from '@angular/router';
import * as data from '../../credentials.json';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(private router: Router) { }

  login(email: string, password: string): Observable<any> {
    const users = (data as any).default;
    if (users.some((user: { email: string; password: string; }) => user?.email === email && user?.password === password)) {
      sessionStorage.setItem('token', 'specialToken');
      return of({});
    }
    return of({error: 'Wrong credentials. Please try again.'});
  }

  register(email: string, password: string): Observable<any> {
    sessionStorage.setItem('token', 'specialToken');
    return of({message: true});
  }

  logout(): void {
    sessionStorage.removeItem('token');
    this.router.navigate(['/login']);
  }

  get token(): (string | null) {
    return sessionStorage.getItem('token') || null;
  }

  isLoggedIn(): boolean {
    return this.token !== undefined && this.token !== null;
  }
}
