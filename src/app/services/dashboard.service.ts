import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ActivatedRoute, Router} from '@angular/router';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  page = 1;
  size = 10;
  filters: any = {};

  constructor(private http: HttpClient, private router: Router, private route: ActivatedRoute) {
  }

  getQuotes(page: number = 1): Observable<any> {
    console.log(12)
    const lastItemIndex = page * this.size;
    let query = '';
    if (this.filters.tags) {
      query += `&tags=${this.filters.tags}`;

    }
    return this.http.get<any>(`https://api.quotable.io/quotes?limit=${this.size}&skip=${lastItemIndex}${query}`
    );
  }

  getTags(): Observable<any> {
    return this.http.get<any>(`https://api.quotable.io/tags?limit=100`);
  }

  getQuoteById(id: string): Observable<any> {
    return this.http.get<any>(`https://api.quotable.io/quotes/${id}`);
  }

  setPage(page: number): void {
    this.page = page;
    const queryParams = {page};
    if (page !== undefined) {
      this.router.navigate([], {
        relativeTo: this.route,
        queryParams,
        queryParamsHandling: 'merge',
        skipLocationChange: false
      });
    }
  }

  setFilters(filters: any): void {
    const queryParams: any = {page: this.page};
    if (filters.tags !== 'All') {
      queryParams.tags = filters.tags;
      this.filters.tags = filters.tags;
    } else {
      queryParams.tags = null;
      this.filters.tags = null;
    }
    this.router.navigate([], {
      relativeTo: this.route,
      queryParams,
      queryParamsHandling: 'merge',
      skipLocationChange: false
    });
  }
}
