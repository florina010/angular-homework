import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DashboardComponent} from './components/dashboard/dashboard.component';
import {IsLoggedinGuard} from './utils/guards/is-loggedin.guard';
import {LoginComponent} from './components/login/login.component';
import {IsLoggedoutGuard} from './utils/guards/is-loggedout.guard';
import {DetailsComponent} from './components/details/details.component';
import {RegisterComponent} from './components/register/register.component';

const routes: Routes = [
  {path: 'login', component: LoginComponent, canActivate: [IsLoggedoutGuard], data: {title: 'Login'}},
  {path: 'register', component: RegisterComponent, canActivate: [IsLoggedoutGuard], data: {title: 'Register'}},
  {path: 'dashboard', component: DashboardComponent, canActivate: [IsLoggedinGuard], data: {title: 'Dashboard'}},
  {path: 'details/:id', component: DetailsComponent, canActivate: [IsLoggedinGuard], data: {title: 'Details'}},
  {path: '**', redirectTo: '/login'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
